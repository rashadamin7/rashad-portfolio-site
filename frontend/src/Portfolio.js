import React, { Component } from "react";
import snippets from './img/Snippets.png';
import raptorsInn from './img/RaptorsInn.png';
import madLibs from './img/MadLibs.png';
import kaozVirtus from './img/Logo.jpg';
import geoQuiz from './img/GeoQuiz.png';
import bank from './img/BankAccount.png';
import colour from './img/ColorFrame.png';
import score from './img/AndroidScoreboard.png';


 
class Portfolio extends Component {


    constructor(props) {
        super(props)
        this.state = {
            portfolio: []
            
        }
        
        
    }
    

    
    componentDidMount() {
        let self = this;
        fetch('http://localhost:4000/portfolio', {
            method: 'GET'
        }).then(function(response) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then(function(data) {
            self.setState({portfolio: data});
        }).catch(err => {
        console.log('caught it!',err);
        })
    }


  render() {
    return (
      <div>
        <h1>Portfolio</h1>
        
        {this.state.portfolio.map(member =>
              
            
              
              <div class="column">
                  
                      <div class="card" >
                      
                          <img src={member.image} class="cardImg" />
                              <div class="cardContainer">
                                  <h2>{member.title}</h2>
                                  <p class="title">{member.sub}</p>
                                  
                  
                                  <button class="button">Press</button>
                                  <br />
                                  <br />
                              </div>
                      </div>
                     
                  </div>
              
            
              
              )}
              
                
       
      </div>
    );
  }
}
 
export default Portfolio;