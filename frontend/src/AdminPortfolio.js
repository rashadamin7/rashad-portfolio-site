import React, {Component, useState} from 'react';


const AdminPortfolio = () => {
    const [title, setTitle] = useState("")
      const [sub, setSub] = useState("")
      const [desc, setDesc] = useState("")
      const [image, setImage] = useState("")

      const portSubmission = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/portfolio', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({title, sub, desc, image})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: `)
        } else {
            alert(`Item has been added!`)
        }
    }

    return(
        <div class="container">
        <h1>Add New Portfolio Item</h1>
            <form onSubmit={portSubmission}>
               
               <div class="formContainer">
                    <label for="title" >Title</label>
                    
                    <input type="text" name="title" id="title" placeholder="Enter title here"  required value={title} onChange={e => setTitle(e.target.value) }/>
                    <br /><br />

                    <label for="sub" >Sub Title</label>
                    
                    <input type="text" name="sub" id="sub" placeholder="Enter subtitle here"  required value={sub} onChange={e => setSub(e.target.value) }/>
                    <br /><br />


                    <label for="desc" >Description</label>
                    
                    <input type="text" name="desc" id="desc" placeholder="Enter description here"  required value={desc} onChange={e => setDesc(e.target.value) }/>
                    <br /><br />


                    

                    <label for="image" >Choose a thumbnail</label>
                    
                    <input type="file" name="image" id="image" placeholder="Upload"  required value={image} onChange={e => setImage(e.target.value) }/>
                    <br /><br />


                    <button class="button">Submit</button>
                  </div>
               
            </form>
</div>

    )



}
export default AdminPortfolio