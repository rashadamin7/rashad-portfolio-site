import React, {Component, useState} from 'react';

const AdminResume = () => {
      const [skill, setSkill] = useState("")

      const [school, setSchool] = useState("")
      const [major, setMajor] = useState("")
      const [degreeType, setDegreeType] = useState("")
      const [years, setYears] = useState("")

      const [position, setPosition] = useState("")
      const [company, setCompany] = useState("")
      const [expYears, setExpYears] = useState("")
      
  
      const skillSubmission = async event => {
          event.preventDefault()
          const response = await fetch('http://localhost:4000/skills', {
              method: 'POST',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
              body: JSON.stringify({skill})
          })
          const payload = await response.json()
          if (response.status >= 400) {
              alert(`Oops! Error: ${payload.message} for fields: `)
          } else {
              alert(`Skill has been added!`)
          }
      }
      
      const eduSubmission = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/edu', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({school, major, degreeType, years})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: `)
        } else {
            alert(`Education has been added!`)
        }
    }

    const expSubmission = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/exp', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({position, company, expYears})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: `)
        } else {
            alert(`Experience has been added!`)
        }
    }

      return (
       <div class="container"> 
        <form onSubmit={skillSubmission}>
               <h1>Add New Skill</h1>
               <div class="formContainer">
                    <label for="skill" >Skill</label>
                    
                    <input type="text" name="skill" id="skill" placeholder="Enter skill here"  required value={skill} onChange={e => setSkill(e.target.value) }/>
                    
                    
                    <button class="button">Submit</button>
                  </div>
               
            </form>
            <br /><br />
            <h1>Add New Education</h1>
            <form onSubmit={eduSubmission}>
               
               <div class="formContainer">
                    <label for="school" >School</label>
                    
                    <input type="text" name="school" id="school" placeholder="Enter school here"  required value={school} onChange={e => setSchool(e.target.value) }/>
                    <br /><br />

                    <label for="major" >Major</label>
                    
                    <input type="text" name="major" id="major" placeholder="Enter major here"  required value={major} onChange={e => setMajor(e.target.value) }/>
                    <br /><br />


                    <label for="degreeType" >Degree Type</label>
                    
                    <input type="text" name="degreeType" id="degreeType" placeholder="Enter degree type here"  required value={degreeType} onChange={e => setDegreeType(e.target.value) }/>
                    <br /><br />


                    <label for="years" >Years</label>
                    
                    <input type="text" name="years" id="years" placeholder="Enter how long you attended here here"  required value={years} onChange={e => setYears(e.target.value) }/>
                    <br /><br />

                    <button class="button">Submit</button>
                  </div>
               
            </form>

            <br /><br />
            <h1>Add New Experience</h1>
            <form onSubmit={expSubmission}>
               
               <div class="formContainer">
                    <label for="position" >Position</label>
                    
                    <input type="text" name="position" id="position" placeholder="Enter position here"  required value={position} onChange={e => setPosition(e.target.value) }/>
                    <br /><br />

                    <label for="company" >Company</label>
                    
                    <input type="text" name="company" id="company" placeholder="Enter company here"  required value={company} onChange={e => setCompany(e.target.value) }/>
                    <br /><br />


                    

                    <label for="expYears" >Years</label>
                    
                    <input type="text" name="expYears" id="expYears" placeholder="Enter how long you worked here"  required value={expYears} onChange={e => setExpYears(e.target.value) }/>
                    <br /><br />

                    <button class="button">Submit</button>
                  </div>
               
            </form>
            </div>  

      )
}
export default AdminResume