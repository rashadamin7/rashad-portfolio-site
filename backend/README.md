# Project

This project handles information posted in a contact form. It is a backend application made up of a RESTful JSON API. Data will  be stored in a JSON file.

# How to Run


- To clone, please use `git clone https://gitlab.com/rashadamin7/course-project.git`
- Use the command `npm install` to make sure all packages are up to date
- Create a folder called Data
- Create a file called users.JSON in the new Data folder
- Create an empty array called users in users.JSON
- Create a file called entires.JSON in the new Data folder
- Create an empty array called entries in entries.JSON
- Use the command `npm start`



