import express from 'express';
const router = express.Router();
import { v4 as uuidv4 } from 'uuid';
import jwt from 'express-jwt';
import * as jwtGenerator from 'jsonwebtoken'
const fs = require('fs')
const passwordHash = require('password-hash')
const cors = require('cors')
const mysql = require('mysql')
const formidable = require('formidable');
const fileUpload = require('express-fileupload');
const app = express();


var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "secret",
  database: "portfolio_site"
});

app.use(fileUpload())




const contactValidationCheck  = (req, res, next) => {

    const requiredContactProperties = ["name","email","phoneNumber", "content"]
    const getMissingContactProps = contact => {
        return requiredContactProperties.filter(property => !contact.hasOwnProperty(property))
    }

    const errors = []
    const {body} = req
    const missingProperties = getMissingContactProps(body).map(missingProperty => `Required field: '${missingProperty}' not found`)

            
                
    if (missingProperties.length) {
        errors.push(...missingProperties)
    }
            
         
            
    if (errors.length) {
        return res.status(400).json( { errors })
    }
    next()

}


const userValidationCheck  = (req, res, next) => {

    const requiredUserProperties = ["name", "password", "email"]
    
    const getMissingUserProps = user => {
        
        return requiredUserProperties.filter(propertyUser => !user.hasOwnProperty(propertyUser))
    }
    
    
    const errorsUser = []
    const {body} = req
    const missingUserProperties = getMissingUserProps(body).map(missingUserProperty => `Required field: '${missingUserProperty}' not found`)

    if (missingUserProperties.length) {
        errorsUser.push(...missingUserProperties)
    }
            
         
            
    if (errorsUser.length) {
        return res.status(400).json( { errorsUser })
    }
    
    next()
    
}

const skillCheck  = (req, res, next) => {

    const requiredSkillProperties = ["skill"]
    const getMissingSkillProps = skill => {
        return requiredSkillProperties.filter(property => !skill.hasOwnProperty(property))
    }

    const errors = []
    const {body} = req
    const missingProperties = getMissingSkillProps(body).map(missingProperty => `Required field: '${missingProperty}' not found`)

            
                
    if (missingProperties.length) {
        errors.push(...missingProperties)
    }
            
         
            
    if (errors.length) {
        return res.status(400).json( { errors })
    }
    next()

    

}


const eduCheck  = (req, res, next) => {

    const requiredEduProperties = ["school","major","degreeType", "years"]
    const getMissingEduProps = edu => {
        return requiredEduProperties.filter(property => !edu.hasOwnProperty(property))
    }

    const errors = []
    const {body} = req
    const missingProperties = getMissingEduProps(body).map(missingProperty => `Required field: '${missingProperty}' not found`)

            
                
    if (missingProperties.length) {
        errors.push(...missingProperties)
    }
            
         
            
    if (errors.length) {
        return res.status(400).json( { errors })
    }
    next()

}

const postEdu = (req, res, next) => {
    var sql = "SELECT * FROM education"
        con.query(sql, function (err, result, fields) {
          if (err) throw err;
          
          console.log(result);
          res.send(JSON.stringify(result));
          return;
        });
    next()
}

const portfolioCheck  = (req, res, next) => {

    const requiredPortfolioProperties = ["title","sub","desc", "image"]
    const getMissingPortfolioProps = portfolio => {
        return requiredPortfolioProperties.filter(property => !portfolio.hasOwnProperty(property))
    }

    const errors = []
    const {body} = req
    const missingProperties = getMissingPortfolioProps(body).map(missingProperty => `Required field: '${missingProperty}' not found`)

            
                
    if (missingProperties.length) {
        errors.push(...missingProperties)
    }
            
         
            
    if (errors.length) {
        return res.status(400).json( { errors })
    }
    next()

}

const expCheck  = (req, res, next) => {

    const requiredExpProperties = ["position","company","expYears"]
    const getMissingExpProps = exp => {
        return requiredExpProperties.filter(property => !exp.hasOwnProperty(property))
    }

    const errors = []
    const {body} = req
    const missingProperties = getMissingExpProps(body).map(missingProperty => `Required field: '${missingProperty}' not found`)

            
                
    if (missingProperties.length) {
        errors.push(...missingProperties)
    }
            
         
            
    if (errors.length) {
        return res.status(400).json( { errors })
    }
    next()

}






router.post('/exp', expCheck, (req, res)=>{


    

    

    var sql= "INSERT INTO `experience` (position, company, years) VALUES ('"+req.body.position+"','"+req.body.company+"','"+req.body.expYears+"')"
    
        
        

        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("1 record inserted");
          return res.status(201).json("Success")
        
        });
      });

router.post('/portfolio', portfolioCheck, (req, res)=>{

    const portTitle = req.body.title
    const portSub = req.body.sub
    const portDesc = req.body.desc
    
     
      

    var sql= "INSERT INTO `portfolio` (title, sub, description, image) VALUES ('"+req.body.title+"','"+ req.body.sub+"','"+ req.body.desc+"','"+ req.body.image+"')"
    
        
        

        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("1 record inserted");
          return res.status(201).json("Success")
        
        });
    
      });


router.post('/skills', skillCheck, (req, res)=>{

    const skillName = req.body.skill
    

    

    var sql = "INSERT INTO `skills` (skills) VALUES ('"+req.body.skill+"')"
    
        
        

        con.query(sql, function (err, result) {
          if (err) throw err;
          //console.log("1 record inserted");
          return res.status(201).json("Success")
        
        });
      });



router.post('/edu', eduCheck, (req, res)=>{

    const eduSchool = req.body.school
    const eduMajor = req.body.major
    const eduDegreeType = req.body.degreeType
    const eduYears = req.body.years
        
    
        
    
        var sql = "INSERT INTO `education` (edu_school, edu_major, edu_degreetype, edu_years) VALUES ('"+eduSchool+"','"+ eduMajor+"','"+ eduDegreeType+"','"+ eduYears+"')"
        
            
            
    
            con.query(sql, function (err, result) {
              if (err) throw err;
              console.log("1 record inserted");
              return res.status(201).json("Success")
            
            });
          });

router.post('/contact_form/entries', contactValidationCheck, (req, res)=>{

    const entryName = req.body.name
    const entryEmail = req.body.email
    const entryPhoneNumber = req.body.phoneNumber
    const entryContent = req.body.content

    

    var sql = "INSERT INTO `entries` (name, email, phoneNumber, entries) VALUES ('"+entryName+"','"+ entryEmail+"','"+ entryPhoneNumber+"','"+ entryContent+"')"
    
        
        

        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("1 record inserted");
          return res.status(201).json("Success")
          
        
        });
      });


   
    
    

    



router.post('/users', userValidationCheck,(req, res)=>{
    
    const userName = req.body.name
    const userPwd = req.body.password
    const userEmail = req.body.email
    
    const hashedPassword = passwordHash.generate(userPwd)

     var sql = "INSERT INTO `users` (user_Name, user_Pwd, user_Email) VALUES ('"+userName+"','"+hashedPassword+"','"+ userEmail+"')"
         con.query(sql, function (err, result) {
           if (err) throw err;
           console.log(result);
           //res.send(JSON.stringify(result));
         });
    
})  
    

router.post('/auth',(req, res)=>{
    
    const {body} = req 
    const email = req.body.email
    const password = req.body.password

    const isPwdTrue = passwordHash.verify(password)


    const JWT_SECRET = "string"


    var sql = 'SELECT * FROM users WHERE user_Email = ' + mysql.escape(email) + 'AND user_Pwd='+ mysql.escape(isPwdTrue);
    con.query(sql, function (err, result) {
        if (err) throw res.status(500);
        let token = jwtGenerator.sign(isPwdTrue, JWT_SECRET);
        return res.status(201).json({token});

        
    });
        

    
   
    
})



router.get('/contact_form/entries', (req, res)=>{

  

    
        var sql = "SELECT * FROM entries"
        con.query(sql, function (err, result, fields) {
          if (err) throw err;
          console.log(result);
          res.send(JSON.stringify(result));
          
        });
      
    

    
    
    
})


router.get('/skills',  (req, res)=>{

    

    
        var sql = "SELECT * FROM skills"
        con.query(sql, function (err, result, fields) {
          if (err) throw err;
          console.log(result);
           res.send(JSON.stringify(result));
        });
      
    

    
    
})

router.get('/exp',  (req, res)=>{

    

    
    var sql = "SELECT * FROM experience"
    con.query(sql, function (err, result, fields) {
      if (err) throw err;
      console.log(result);
       res.send(JSON.stringify(result));
    });
  




})


router.get('/edu', (req, res)=>{



    
    var sql = "SELECT * FROM education"
    con.query(sql, function (err, result, fields) {
      if (err) throw err;
      console.log(result);
      res.send(JSON.stringify(result));
    });
  




})

router.get('/portfolio', (req, res)=>{



    
    var sql = "SELECT * FROM portfolio"
    con.query(sql, function (err, result, fields) {
      if (err) throw err;
      console.log(result);
      res.send(JSON.stringify(result));
    });

    
  




})

router.delete('/contact_form/entries', (req, res)=>{

    
    
    

    
    var sql = "DELETE FROM entries WHERE id='"+req.body.id+"'"
    con.query(sql, function (err, result, fields) {
      if (err) throw err;
      console.log(result);
      res.send(JSON.stringify(result));
    });
  




})



router.get('/contact_form/entries/:id', (req, res)=>{
    const userID = req.params.id
    fs.readFile('./data/entries.json', 'utf8', function (err,data) {
        if (err) throw err
        
        const verifyArrayOfEntriesID = JSON.parse(data)
        const foundID = verifyArrayOfEntries.entries.find(foundID => foundEmail.id === userID);
        console.log(foundID)
    
        if (userID === foundID.id) {
            
            return res.status(201).json(foundID);
        }
        
        return res.status(400).json({error: "ID not found"})
    });  


    } )

export default router;

