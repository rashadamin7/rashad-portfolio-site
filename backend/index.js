import express from 'express'
import routes from './src/routes.js'
import cors from "cors";


const app = express();

app.use(cors());
const port = process.env.PORT || 4000;

app.use(express.json());
app.use('/', routes);

export default app.listen(port, function () {
  console.log(`Express server listening on port ${port}.`);
});
