# CourseProject

Rashad Amin's Portfolio site

Login: http://localhost:3000/#/Login (Create a User First)

Admin Pages: http://localhost:3000/#/Admin,
http://localhost:3000/#/AdminPortfolio,
http://localhost:3000/#/AdminResume   

**_DB CREATION_**

Create tables using sql.sql

**_Issues_**

- Currently, the resume page is only able to out put skills, had a little difficulites displaying multiple tables
- Images aren't displaying on portfolio page
- Deletion of entries on admin panel only works through postman
