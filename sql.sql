CREATE TABLE `education` (
  `edu_id` int NOT NULL AUTO_INCREMENT,
  `edu_school` varchar(45) NOT NULL,
  `edu_major` varchar(45) NOT NULL,
  `edu_degreetype` varchar(100) NOT NULL,
  `edu_years` varchar(10) NOT NULL,
  PRIMARY KEY (`edu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phoneNumber` varchar(16) DEFAULT NULL,
  `entries` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `entries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phoneNumber` varchar(16) DEFAULT NULL,
  `entries` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `resume` (
  `_id` int NOT NULL AUTO_INCREMENT,
  `skill` varchar(15) DEFAULT NULL,
  `expPosition` varchar(45) DEFAULT NULL,
  `expCompany` varchar(60) DEFAULT NULL,
  `eduSchool` varchar(45) DEFAULT NULL,
  `eduMajor` varchar(45) DEFAULT NULL,
  `eduDegreeType` varchar(420) DEFAULT NULL,
  `years` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `skills` (
  `idskills` int NOT NULL AUTO_INCREMENT,
  `skills` varchar(45) NOT NULL,
  PRIMARY KEY (`idskills`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_Name` varchar(18) NOT NULL,
  `user_pwd` varchar(100) NOT NULL,
  `user_email` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `portfolio_site`.`users`
(`user_id`,
`user_Name`,
`user_pwd`,
`user_email`)
VALUES
(<{user_id: }>,
<{user_Name: }>,
<{user_pwd: }>,
<{user_email: }>);
